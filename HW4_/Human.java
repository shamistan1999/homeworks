package HW4_;

import java.util.Arrays;

public class Human {

    /***
     In class`Human` create constructors:
     - constructor which describes the name, surname and the date of birth
     - constructor which describes the name, surname, date of birth, father and mother
     - constructor which describes all the fields
     - empty constructor
     * */


    String name="dfdfdfdf";
    String surname="wfdfd";
    int year=1999;
    int iq=60;
    Pet pet;
    Human mother;
    Human father;
    String [][]schedule= new String[6][2];


    Pet pt = new Pet("blue", "micky");
    public Human(String name, String surname, int year)
    {
        this.name=name;
        this.surname=surname;
        this.year=year;
    }
    public Human(String name, String surname)
    {
        this.name=name;
        this.surname=surname;
    }
    public Human(String name, String surname, int year, Human father, Human mother)
    {
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.father=father;
        this.mother=mother;
    }
    public Human(String name, String surname, int year, Human father, Human mother,int iq, Pet pet, String [][]schedule)
    {
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.father=father;
        this.mother=mother;
        this.mother=mother;
        this.iq=iq;
        this.pet=pet;
        this.schedule=schedule;
    }

    public Human(){

    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.deepToString(schedule) +
                ", pt=" + pt +
                '}';
    }

    public void greetPet() {
        System.out.println("Hello, " + pt.nickname);
    }

    public void describePet() {

        if(pt.tricklevel>50)
        {
            System.out.println("I have a " + pt.species + ", he is " + pt.age + " years old, he is very sly");
        }
        else
        {
            System.out.println("I have a " + pt.species + ", he is " + pt.age + " years old, he is almost not sly");
        }

    }

}
